#!/bin/bash
input="/data/users/yihuilai/CMSSW_10_3_1_patch1/src/HGCalTileSim/condor/suball_hg.sh"
num=0
restrict=200
totaln=0
while IFS= read -r line
do
  $line
  sleep 0.5
  totaln=$(( $totaln + 1 ))
  num=`condor_q yihuilai |wc -l`
  num=$(( $num - 7  ))
  while [ $num -gt $restrict ]
  do
    echo "Current job: $num, too much, wait..."
    num=`condor_q yihuilai |wc -l`
    num=$(( $num - 7  ))
    echo "current ====> $totaln"
    sleep 300
    #num=0
    #break
  done
done < "$input"
