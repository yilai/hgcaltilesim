#!/bin/sh

python3 SubmitJobs.py --beamx 0 --beamy 0 -L 30 -w 15 -W 1.3 -t 3.8 -r 4.806 -d 2.2 -b 3.175 -m 0.985 -p 0.7 -N 500 -n 1 -i 1
python3 SubmitJobs.py --beamx 0 --beamy 0 -L 30 -w 15 -W 1.3 -t 3.8 -r 4.806 -d 2.2 -b 3.175 -m 0.985 -p 0.7 -N 500 -n 2 -i 1
python3 SubmitJobs.py --beamx 0 --beamy 0 -L 30 -w 15 -W 1.3 -t 3.8 -r 4.806 -d 2.2 -b 3.175 -m 0.985 -p 0.7 -N 500 -n 3 -i 1
python3 SubmitJobs.py --beamx 0 --beamy 0 -L 30 -w 15 -W 1.3 -t 3.8 -r 4.806 -d 2.2 -b 3.175 -m 0.97 -p 0.7 -N 500 -n 1 -i 0
python3 SubmitJobs.py --beamx 0 --beamy 0 -L 30 -w 15 -W 1.3 -t 3.8 -r 4.806 -d 2.2 -b 3.175 -m 0.97 -p 0.7 -N 500 -n 2 -i 0
python3 SubmitJobs.py --beamx 0 --beamy 0 -L 30 -w 15 -W 1.3 -t 3.8 -r 4.806 -d 2.2 -b 3.175 -m 0.97 -p 0.7 -N 500 -n 3 -i 0


python3 SubmitJobs.py --beamx 0 --beamy 0 -L 40 -w 20 -W 1.3 -t 3.8 -r 4.806 -d 2.2 -b 3.175 -m 0.985 -p 0.7 -N 500 -n 1 -i 1
python3 SubmitJobs.py --beamx 0 --beamy 0 -L 40 -w 20 -W 1.3 -t 3.8 -r 4.806 -d 2.2 -b 3.175 -m 0.985 -p 0.7 -N 500 -n 2 -i 1
python3 SubmitJobs.py --beamx 0 --beamy 0 -L 40 -w 20 -W 1.3 -t 3.8 -r 4.806 -d 2.2 -b 3.175 -m 0.985 -p 0.7 -N 500 -n 3 -i 1
python3 SubmitJobs.py --beamx 0 --beamy 0 -L 40 -w 20 -W 1.3 -t 3.8 -r 4.806 -d 2.2 -b 3.175 -m 0.97 -p 0.7 -N 500 -n 1 -i 0
python3 SubmitJobs.py --beamx 0 --beamy 0 -L 40 -w 20 -W 1.3 -t 3.8 -r 4.806 -d 2.2 -b 3.175 -m 0.97 -p 0.7 -N 500 -n 2 -i 0
python3 SubmitJobs.py --beamx 0 --beamy 0 -L 40 -w 20 -W 1.3 -t 3.8 -r 4.806 -d 2.2 -b 3.175 -m 0.97 -p 0.7 -N 500 -n 3 -i 0


python3 SubmitJobs.py --beamx 0 --beamy 0 -L 50 -w 25 -W 1.3 -t 3.8 -r 4.806 -d 2.2 -b 3.175 -m 0.985 -p 0.7 -N 500 -n 1 -i 1
python3 SubmitJobs.py --beamx 0 --beamy 0 -L 50 -w 25 -W 1.3 -t 3.8 -r 4.806 -d 2.2 -b 3.175 -m 0.985 -p 0.7 -N 500 -n 2 -i 1
python3 SubmitJobs.py --beamx 0 --beamy 0 -L 50 -w 25 -W 1.3 -t 3.8 -r 4.806 -d 2.2 -b 3.175 -m 0.985 -p 0.7 -N 500 -n 3 -i 1
python3 SubmitJobs.py --beamx 0 --beamy 0 -L 50 -w 25 -W 1.3 -t 3.8 -r 4.806 -d 2.2 -b 3.175 -m 0.97 -p 0.7 -N 500 -n 1 -i 0
python3 SubmitJobs.py --beamx 0 --beamy 0 -L 50 -w 25 -W 1.3 -t 3.8 -r 4.806 -d 2.2 -b 3.175 -m 0.97 -p 0.7 -N 500 -n 2 -i 0
python3 SubmitJobs.py --beamx 0 --beamy 0 -L 50 -w 25 -W 1.3 -t 3.8 -r 4.806 -d 2.2 -b 3.175 -m 0.97 -p 0.7 -N 500 -n 3 -i 0



