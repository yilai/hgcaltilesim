#!/bin/bash
input="/data/users/yihuilai/CMSSW_10_3_1_patch1/src/HGCalTileSim/condor/results_50mm/condor/suball_hg.sh"
num=0
restrict=200
totaln=0
while IFS= read -r line
do
  $line
  totaln=$(( $totaln + 1 ))
  num=`condor_q yihuilai |wc -l`
  num=$(( $num - 7  ))
  while [ $num -gt $restrict ]
  do
    echo "Current job: $num, too much, wait..."
    sleep 300
    num=`condor_q yihuilai |wc -l`
    num=$(( $num - 7  ))
    echo "current ====> $totaln"
    #num=0
    #break
  done
done < "$input"
